
# 12 column responsive grid system

This is a basic implementation of a 12 column responsive grid system that uses css grid to lay out content.

This grid system should be used to lay out components of the page that designed to fit on a grid. Inside a component you should use css grid directly, or flexbox, whatever is the best for the job.

This grid system tries to be as compatible with flex layout as possible. See [media.md](media.md) for details.
 
## Usage

Normally you will have a container, and a set of items inside that container. Your goal is to align those items inside the container so that they would be placed on a grid.

```html
<div class='container'>
  <div class='item item1'></div>
  <div class='item item2'></div>
  <div class='item item3'></div>
  <div class='item item4'></div>
</div>
```

Basically your workflow is:

 - Apply grid-container on the container.
 - Optionally apply grid-container modifiers to customize how the items are layed out
 - Optionally apply grid-item modifiers on items to customize sizing, placement, and how many grid columns or rows that item takes.

## Setting up the container

In order to set up the container you need to apply the `grid-container` mixin on it.

```scss
.container {
  @include grid-container();
}
```

This already achieves a lot:
 - Sets up 12, 8, 6 or 4 columns depending on the screen size
 - Stretches items to fill the grid cell they occupy.
 - Sets up a gap between grid cells (based on screen size) and apply the same amount of padding.


### Container customization

You may use the following mixins to customize how the grid is layed out. 

#### grid-container-columns(n)

If applied the grid will have exactly the specified amount of columns.

#### grid-container-equal-rows()

By default the height of the rows of the grid could be different, because each row is as tall as the tallest cell in that row. If you want all rows to have equal size use this mixin to make each row as tall as the tallest cell in the whole grid.

#### grid-container-gap-auto()

If applied, the grid gap will be chosen according to screen size (16 or 24px)
This is the default behaviour.

#### grid-container-gap(px)

With this mixing you can force the grid gap to be the specified amount. Useful for disabling grid gap with `grid-container-gap(0)`

#### grid-container-padding(px)

With this mixing you can force the padding around the grid to be exactly the specified amount.

#### grid-container-padding-auto(px)

With this mixing you can set the padding to be automatically chosen (it will be the same as grid gap, and it will change as grid-gap changes.

#### grid-container-width-fluid()

This is how the grid works by default. The columns are stretched out to fill all the available space. 

#### grid-container-width(px)

The same as grid-container-fix, but you can specify the exact width you want.

#### grid-container-max-width(px)

The same as grid-container-width, but sets the max-width property. Useful if you need a fluid container contstrained to a max width.


## Setting up a grid item

You don't need to apply any mixins to an item to set it up as a grid item. By default each item will take one cell, and it will stretch over it.

### Grid item customization

#### Sizing with grid-item-columns(n) and grid-item-rows(n)

**Important: This is not ready **

With the mixins `grid-item-columns(n)` and `grid-item-rows(n)` you can specify how many cells the grid item should take horizontally and vertically.

For columns, you can specify a negative number, or zero. Zero means the item will take all columns in the row. That can mean 12, 8, 6 or 4, depending on the screen size. A negative number means the item will take all but the specified amount of columns. Meaning, on a screen with 12 columns, `grid-item-columns(-2)` will take 10 columns. 
 
#### Specifying aspect ratio with grid-item-aspect-ratio(ratio)

You can enforce an aspect ratio on an item with applying the `grid-item-aspect-ratio(ratio)` mixin, where ratio is a number, for example 16 / 9, or 0.5, etc.  

Important: The height of the row is decided by the tallest item in the row. If you put two items next to each other, one having 1/1 and one having 2/1 aspect ratio, the taller one dictates the height of the row, and the shorter one will 
 stretch to fill the row.
 
## Advanced usage 

The grid system uses css grid to lay out content. For advanced configurations which can't be done directly by mixins, feel free to apply css grid related properties on your container or items.

There is one catch. For properties that can be configured via mixins, make sure you use the mixins instead of directly applying them. For example: Never apply width, or padding directly on a grid container. Use grid-container-fix, grid-container-padding, grid-container-width, etc to set them.  

## Etc

Size of N grid column at specific breakpoints:

| Size   | PX    | 1   | 2   | 3   | 4   | 5     | 6     | 7     | 8     | 9     | 10    | 11    | 12    | 
|--------|------:|----:|----:|----:|----:|------:|------:|------:|------:|------:|------:|------:|------:| 
| xxsMax | 419   | 35  | 70  | 105 | 140 | 175   | 210   | 244   | 279   | 314   | 349   | 384   | 419   | 
| xsMax  | 599   | 50  | 100 | 150 | 200 | 250   | 300   | 349   | 399   | 449   | 499   | 549   | 599   | 
| smMax  | 959   | 80  | 160 | 240 | 320 | 400   | 480   | 559   | 639   | 719   | 799   | 879   | 959   | 
| mdMax  | 1,279 | 107 | 213 | 320 | 426 | 533   | 640   | 746   | 853   | 959   | 1,066 | 1,172 | 1,279 | 
| lgMax  | 1,439 | 120 | 240 | 360 | 480 | 600   | 720   | 839   | 959   | 1,079 | 1,199 | 1,319 | 1,439 | 
| xlMax  | 1,439 | 120 | 240 | 360 | 480 | 600   | 720   | 839   | 959   | 1,079 | 1,199 | 1,319 | 1,439 | 
| xxlMax | 2,600 | 217 | 433 | 650 | 867 | 1,083 | 1,300 | 1,517 | 1,733 | 1,950 | 2,167 | 2,383 | 2,600 | 
