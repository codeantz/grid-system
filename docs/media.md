# How to use media queries

## Simple query

Using the media mixin you can target one screen size (ranges from a minimum and maximum size) as this example shows.

```scss
@include media('mobile.m') {
  margin: 12px;
}
```

The query above will be applied to when the screen size (window size) is between the minimum size specified for `mobile.m` and the maximum.

Possible values for screen sizes come from `_config.scss` where a list defines breakpoints and names of the ranges between. 

The names of screen sizes defined in the config assumes portrait mode. For example `tablet.s`, which means small tablet does in fact target small tablets, but also targets large mobiles in portrait mode too because the width of a large mobile is approx the same.   

This table may help:

| Value     | Min size | Max size | Meaning                                   |
|-----------|---------:|---------:|-------------------------------------------|
| mobile.s  |        0 |      359 | Small  phone                              |
| mobile.m  |      360 |      399 | Medium phone                              |
| mobile.l  |      400 |      479 | Large phone                               |
| phablet.s |      480 |      599 | Large phone, small phone in landscape     |
| phablet.m |      600 |      719 | Phablet, small tablet, phone in landscape |
| phablet.l |      720 |      839 | Large phone, small tablet, phablet        |
| tablet.s  |      840 |      959 | Large phone, small tablet, phablet        |

// TODO: Finish this table with all values

## Ranges

Specifying each range one by one would be cumbersome. You can use `from` and `to` operators to specify a range of screen sizes in one go.

You may specify a range in the format of `from XXX to YYY`. For example `from desktop.s to desktop.l` targets desktop s, m, and l sizes.

```scss
@include media('from desktop.s to desktop.l') {
  // ...
}
```

You may use only `from XXX` or `to XXX` to specify a range that only has a minimum or a maximum.

```scss
@include media('to desktop.s') {
  // All screen sizes will be covered from zero to the maximum of desktop.s
  // Meaning this covers desktop.s and smaller sizes
}
```

```scss
@include media('from tablet.l') {
  // All screen sizes will be covered from the minimum of tablet.l to infinity.
  // Meaning, this covers tablet.l and bigger sizes
}
```

## Debug

You may use the mixin `media-debug()` to display the name of the current breakpoint at the top left corner of the viewport. 

Ideally you should apply it on `body`, but that's not possible from component stylesheet where it's most convenient to use this feature. Feel free to apply on any element, just beware that it will override some styles the :after pseudo-element.