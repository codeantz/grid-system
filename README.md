# Simple Grid System

See 
 - Media queries: [docs/media.md](docs/media.md)
 - Grid mixins: [docs/media.md](docs/grid.md)

## How to use:

Install via npm:

    npm install --save git+https://example.com/acme/grid-system.git

And request either media.scss or grid.scss in your sass files:


    @import '~grid-system/media.scss';
    @import '~grid-system/grid.scss';


## Planned features

 - Set alignment on container, items in container, or individual items.
 - Set aspect ratio on items in container